# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

### Changes
...

## [0.4.0] - 2023-12-10

### Removes

- `szew.h2.legacy-spec` and `legacy-defaults`, as H2 no longer allows these.

### Fixes

- `bulk-insert-mode!` and `retrieval-mode!` aligned with available H2 opts.

### Adds

- `set-opts!` helper that executes each option separately and captures return.

## [0.3.3] - 2021-12-14

### Changes

- Dependency upgrades, stopped testing against Clojure 1.9.
- H2 1.4.202 no longer accepts EARLY_FILTER for MVStore, removed from defaults.

## [0.3.2] - 2020-05-28

### Fixes

- Database defaults for H2 1.4.200 (MVCC flag was removed).

### Changes

- Moved library dependencies to `:provided`.

## [0.3.1] - 2020-02-01

### Changes

- Ongoing documentation touchups.
- Version bumps.

### Added

- `szew.h2.server` wrapper for `org.h2.tools.Server` class.

## [0.3.0] - 2019-06-30

### Changes

- Extended `spec` and introduced `legacy-spec`.
- Added `next.jdbc` support, kept `java.jdbc`.
- BREAKING: Killed global state around `szew.h2/connections`.
- BREAKING: split helper functions into `java.jdbc` and `next.jdbc`.
- BREAKING: split misc helpers into `szew.h2.util` namespace.
### Added

- Introduced `spec->URL`, `spec->DS` and `spec->CP`.

## [0.2.1] - 2019-03-06

### Changes

- Clojure 1.10.0, H2 1.4.198, `java.jdbc` 0.7.9
- Improved input and output error handling for `spec`.

### Added

- The `dispose!` finalizer and `adjust!` modifier operations.

## [0.2.0] - 2018-02-11

### Changes

- Clojure 1.9.0 and `spec.alpha`, defined specs for spec and opts.
- Added DEFRAG_ALWAYS option set to FALSE to defaults.

## [0.1.2] - 2017-05-22

- Fixes and updates in `conn!` and `make!`
- Formatting and style fixes.
- New version of H2: 1.4.195.

## [0.1.1] - 2016-11-01

- New version of H2: 1.4.193.
- `h2/spec` extended with: nioMemFS, nioMemLZF and split filesystems.
- `copy!` can now use temporary file that's deleted afterwards.

## [0.1.0] - 2016-10-01
### Initial

- This is the first extraction from private `szew` toolkit.
- `szew.h2` namespace defines H2 connections specs and datasources.

[Unreleased]: https://bitbucket.org/spottr/szew-h2/branches/compare/master%0D0.4.0#diff
[0.4.0]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.4.0%0D0.3.3#diff
[0.3.3]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.3.3%0D0.3.2#diff
[0.3.2]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.3.2%0D0.3.1#diff
[0.3.1]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.3.1%0D0.3.0#diff
[0.3.0]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.3.0%0D0.2.1#diff
[0.2.1]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.2.1%0D0.2.0#diff
[0.2.0]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.2.0%0D0.1.2#diff
[0.1.2]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.1.2%0D0.1.1#diff
[0.1.1]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.1.1%0D0.1.0#diff
[0.1.0]: https://bitbucket.org/spottr/szew-h2/branches/compare/0.1.0%0D2a3124ffeadcf045b7e8644350a0065ebdc5d9a0#diff

