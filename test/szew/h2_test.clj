; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database connection setup helper functions tests."}
  szew.h2-test
  (:require
    [szew.h2 :refer [spec spec->URL spec->DS spec->CP]]
    [clojure.test :refer [deftest testing is]]
    [orchestra.spec.test :as st])
  (:import
    [java.io File]
    [org.h2.jdbcx JdbcDataSource JdbcConnectionPool]))

(st/instrument)

(defn file-please!
  "Gimme a temporary File.
  "
  []
  (io! "Mr Java.Io.File, bring me a File!"
    (doto (File/createTempFile "szew-io-test-" ".bin")
      (.deleteOnExit))))

(defn thanks-for-the-file!
  "Delete a File.
  "
  [^File f]
  (when f
    (.delete f)))

(def opts {:method   :default
           :user     "some username"
           :password "some password"
           :hostname "superhost"
           :port     9999
           :part     30
           :split?   false
           :flags    {"COMPRESS" "FALSE"}})

(deftest Spec-creation-testing

  (testing "Creating :raw connection spec."
    ;; Connection string used in subname directly
    (let [{:keys [classname subprotocol subname user password]}
          (spec "PATH" (assoc opts :method :raw))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "PATH"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password]}
          (spec "PATH" (assoc opts :method :raw :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "PATH"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :tcp connection spec."
    ;; TCP spec
    (let [{:keys [classname subprotocol subname user password]}
          (spec "PATH" (assoc opts :method :tcp))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "tcp://superhost:9999/PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password]}
          (spec "PATH" (assoc opts :method :tcp :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "tcp://superhost:9999/PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :mem connection spec."
    ;; In-memory database, persistent until JVM shutdowns
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :mem))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "mem:PATH;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :mem :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "mem:PATH;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :nio-mem-fs connection spec."
    ;; New in-memory database, persistent until JVM shutdowns
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :nio-mem-fs))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "nioMemFS:PATH;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :nio-mem-fs :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "nioMemFS:PATH;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :nio-mem-lzf connection spec."
    ;; In-memory database, persistent until JVM shutdowns
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :nio-mem-lzf))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "nioMemLZF:30:PATH;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :nio-mem-lzf :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "nioMemLZF:30:PATH;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :nio-mapped connection spec."
    ;; Memory mapped nio local database
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :nio-mapped))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "split:30:nioMapped:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :nio-mapped :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "split:30:nioMapped:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :raf connection spec."
    ;; RandomAccessFile local database
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :raf))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :raf :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "split:30:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :async connection spec."
    ;; AsyncChannel local database
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :async))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "async:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :method :async :split? true))]
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "split:30:async:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password"))))

  (testing "Creating :default / :nio connection spec."
    ;; Default spec, nio
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" opts)]
      (is (= db (spec "PATH" (assoc opts :method :nio))))
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "nio:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))
    ;; same, but with split enabled
    (let [{:keys [classname subprotocol subname user password] :as db}
          (spec "PATH" (assoc opts :split? true))]
      (is (= db (spec "PATH" (assoc opts :method :nio :split? true))))
      (is (= classname "org.h2.Driver"))
      (is (= subprotocol "h2"))
      (is (= subname "split:30:nio:PATH;COMPRESS=FALSE"))
      (is (= user "some username"))
      (is (= password "some password")))))

(deftest Spec-transformation-testing
  (testing "Transforming spec to URL, DataSource and ConnectionPool."
    (let [a-spec (spec "ds-mem" {:method   :mem
                                 :hostname "localhost"
                                 :port     7777
                                 :user     "test"
                                 :password "magic"
                                 :flags  {"COMPRESS" "FALSE"}})
          url    "jdbc:h2:mem:ds-mem;DB_CLOSE_DELAY=-1;COMPRESS=FALSE"
          a-ds   (spec->DS a-spec)
          a-cp   (spec->CP a-spec)]
      (is (= url (spec->URL a-spec)))
      ;; Data Source introspection
      (is (= url (.getURL ^JdbcDataSource a-ds)))
      (is (= (:user a-spec) (.getUser ^JdbcDataSource a-ds)))
      (is (= (:password a-spec) (.getPassword ^JdbcDataSource a-ds)))
      ;; Connection Pool introspections available
      (doto ^JdbcConnectionPool a-cp
        (.setLoginTimeout 120)
        (.setMaxConnections 30))
      (is (= 120 (.getLoginTimeout ^JdbcConnectionPool a-cp)))
      (is (= 30 (.getMaxConnections ^JdbcConnectionPool a-cp)))
      (.dispose ^JdbcConnectionPool a-cp))))
