; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database nect.jdbc administration helpers tests."}
 szew.h2.next-jdbc-test
 (:require
   [next.jdbc :as jdbc]
   [next.jdbc.sql :as jdbc.sql]
   [szew.h2 :refer [spec spec->URL]]
   [szew.h2.next-jdbc
    :refer [set-opts! raze! copy! pump! dump!]]
   [szew.h2.util :refer [de-clob]]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st]
   [szew.h2-test :refer [file-please! thanks-for-the-file!]])
 (:import
   [java.io File]))

(st/instrument)

(deftest Roundtripping-test-with-file-store-databases-next-jdbc

  (let [schema "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"
        record {:X/A "A" :X/B "B" :X/C "C"}
        boop!  (fn [conn]
                 (jdbc/execute! conn [schema])
                 (jdbc.sql/insert! conn :x record)
                 (first (jdbc.sql/query conn ["SELECT * FROM X"])))]
    (try
      (testing "Testing :mem"
        (let [tmp (file-please!) 
              db  (spec->URL (spec (.getName ^File tmp) {:method :mem}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mem-fs"
        (let [tmp (file-please!) 
              db  (spec->URL (spec (.getName ^File tmp)
                                   {:method :nio-mem-fs}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mem-lzf"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getName ^File tmp)
                                   {:method :nio-mem-lzf}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mapped"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :nio-mapped}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mapped with split enabled"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :nio-mapped :split? true}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :raf"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :raf}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :raf with split enabled"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :raf :split? true}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :async"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp) {:method :async}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :async with split enabled"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :async :split? true}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp) {:method :nio}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio with split"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :nio :split? true}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :default"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :default}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :default with split"
        (let [tmp (file-please!)
              db  (spec->URL (spec (.getCanonicalPath tmp)
                                   {:method :default :split? true}))]
          (is (= {:X/A "A" :X/B "B" :X/C "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp))))))

(deftest Database-option-setting
  (let [tmp (file-please!)
        db (spec (.getCanonicalPath tmp) {:method :default})]
    (jdbc/with-transaction [tx (spec->URL db)]
      (is (nil? (set-opts! tx {})))
      (let [bm (set-opts! tx {"LOCK_MODE" 0 "WRITE_DELAY" 0})
            rm (set-opts! tx {"LOCK_MODE" 3 "WRITE_DELAY" 500})
            xx (set-opts! tx {"BADOPT" 9001})]
        (is (= 2 (count bm)))
        (is (= {:next.jdbc/update-count 0} (get bm "SET LOCK_MODE 0")))
        (is (= {:next.jdbc/update-count 0} (get bm "SET WRITE_DELAY 0")))
        (is (= 2 (count rm)))
        (is (= 1 (count xx)))
        (is (instance? Exception (get xx "SET BADOPT 9001")))))
    (thanks-for-the-file! tmp)))

(deftest Misc-helpers-testing-next-jdbc

  (testing "De-clobbing test."
    (let [db (spec "de-clob-next-jdbc" {:method :mem})]
      (jdbc/with-transaction [tx (spec->URL db)]
        (let [schema "CREATE TABLE X (A VARCHAR, B CLOB, C CLOB);"
              record {:X/A "A" :X/B "B" :X/C "C"}
              _      (jdbc/execute! tx [schema])
              _      (jdbc.sql/insert! tx :x record)
              read1  (first (jdbc.sql/query tx ["SELECT * FROM X"]))
              read2  (de-clob read1 [:X/B]) ; one key
              read3  (de-clob read1)]     ; all keys
          (is (= (:X/A read1) "A"))
          (is (instance? java.sql.Clob (:X/B read1)))
          (is (instance? java.sql.Clob (:X/C read1)))
          (is (= (:X/A read2) "A"))
          (is (= (:X/B read2) "B"))
          (is (instance? java.sql.Clob (:X/C read2)))
          (is (= (:X/A read3) "A"))
          (is (= (:X/B read3) "B"))
          (is (= (:X/C read3) "C"))))
      (raze! (spec->URL db)))))

(deftest Pump-n-dump-testing-uncompressed-out-uncompressed-in-next-jdbc

  (let [tmp (file-please!)]
    (try
      (let [dmp    (spec->URL (spec "dmp-next-jdbc" {:method :mem}))
            imp    (spec->URL (spec "imp-next-jdbc" {:method :mem}))
            schema  "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
        (jdbc/execute! dmp [schema])
        (jdbc.sql/insert! dmp :x {:X/A "A" :X/B "B" :X/C "C"})
        (dump! dmp (.getCanonicalPath tmp) false)
        (pump! imp (.getCanonicalPath tmp) false)
        (is (= (first (jdbc.sql/query dmp ["SELECT * FROM X"]))
               (first (jdbc.sql/query imp ["SELECT * FROM X"]))))
        (do (raze! dmp)
            (is (thrown? Exception
                         (first (jdbc.sql/query dmp ["SELECT * FROM X"]))))))
      (finally
        (thanks-for-the-file! tmp)))))

(deftest Pump-n-dump-testing-compressed-out-compressed-in-next-jdbc

  (let [tmp (file-please!)]
    (try
      (let [dmp    (spec->URL (spec "dmp-gz-next-jdbc" {:method :mem}))
            imp    (spec->URL (spec "imp-gz-next-jdbc" {:method :mem}))
            schema  "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
        (jdbc/execute! dmp [schema])
        (jdbc.sql/insert! dmp :x {:X/A "A" :X/B "B" :X/C "C"})
        (dump! dmp (.getCanonicalPath tmp) true)
        (pump! imp (.getCanonicalPath tmp) true)
        (is (= (first (jdbc.sql/query dmp ["SELECT * FROM X"]))
               (first (jdbc.sql/query imp ["SELECT * FROM X"]))))
        (do (raze! dmp)
            (is (thrown? Exception
                         (first (jdbc.sql/query dmp ["SELECT * FROM X"]))))))
      (finally
        (thanks-for-the-file! tmp)))))

(deftest Copy-test-no-pump-n-dump-next-jdbc
  (let [tmp (file-please!)]
    (try
      (let [orig   (spec->URL (spec "orig-copy-next-jdbc" {:method :mem}))
            dest   (spec->URL (spec "dest-copy-next-jdbc" {:method :mem}))
            schema "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
        (jdbc/execute! orig [schema])
        (jdbc.sql/insert! orig :x {:X/A "A" :X/B "B" :X/C "C"})
        (copy! orig dest (.getCanonicalPath tmp) true)
        (is (= (first (jdbc.sql/query orig ["SELECT * FROM X"]))
               (first (jdbc.sql/query dest ["SELECT * FROM X"]))))
        (raze! orig)
        (is (thrown? Exception
                     (first (jdbc.sql/query orig ["SELECT * FROM X"])))))
      (finally
        (thanks-for-the-file! tmp)))))

(deftest Copy-test-no-pump-n-dump-no-file-next-jdbc
  (let [orig   (spec->URL (spec "orig-copy-no-file-next-jdbc" {:method :mem}))
        dest   (spec->URL (spec "dest-copy-no-file-next-jdbc" {:method :mem}))
        schema "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
    (jdbc/execute! orig [schema])
    (jdbc.sql/insert! orig :x {:X/A "A" :X/B "B" :X/C "C"})
    (copy! orig dest true) 
    (is (= (first (jdbc.sql/query orig ["SELECT * FROM X"]))
           (first (jdbc.sql/query dest ["SELECT * FROM X"]))))
    (raze! orig)
    (is (thrown? Exception
                 (first (jdbc.sql/query orig ["SELECT * FROM X"]))))))
