; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database server mode functions tests."}
 szew.h2.server-test
 (:require
   [szew.h2.server :as server]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st])
 (:import
   [org.h2.tools Server]))

(st/instrument)

;; Some tests will be java.jdbc or next.jdbc specific, look there.

(deftest PG-server-ops
  (testing "Creation properties"
    (let [pg (server/create-pg-server)]
      (is (instance? Server pg))
      (is (number? (server/get-port pg)))
      (is (not (nil? (server/get-service pg))))
      (is (string? (server/get-status pg)))
      (is (= "Not started" (server/get-status pg)))
      (is (string? (server/get-URL pg)))
      (is (re-matches #"^pg://[^:]+:\d+$"
           (server/get-URL pg)))
      (is (false? (server/is-running? pg)))
      (let [pg2 (server/start! pg)]
        (is (= pg pg2))
        (is (true? (server/is-running? pg2)))
        (is (re-matches #"^PG server running at pg://.*$"
                        (server/get-status pg2)))
        (is (= pg2 (server/stop! pg2)))
        (is (= "Not started" (server/get-status pg2)))
        (server/start! pg2)
        (is (re-matches #"^PG server running at pg://.*$"
                        (server/get-status pg2)))
        (server/stop! pg2)))))

(deftest TCP-server-ops
  (testing "Creation properties"
    (let [tcp (server/create-tcp-server)]
      (is (instance? Server tcp))
      (is (number? (server/get-port tcp)))
      (is (not (nil? (server/get-service tcp))))
      (is (string? (server/get-status tcp)))
      (is (= "Not started" (server/get-status tcp)))
      (is (string? (server/get-URL tcp)))
      (is (re-matches #"^tcp://[^:]+:\d+$"
           (server/get-URL tcp)))
      (is (false? (server/is-running? tcp)))
      (let [tcp2 (server/start! tcp)]
        (is (= tcp tcp2))
        (is (true? (server/is-running? tcp2)))
        (is (re-matches #"^TCP server running at tcp://.*$"
                        (server/get-status tcp2)))
        (is (= tcp2 (server/stop! tcp2)))
        (is (= "Not started" (server/get-status tcp2)))
        (server/start! tcp2)
        (is (re-matches #"^TCP server running at tcp://.*$"
                        (server/get-status tcp2)))
        (server/stop! tcp2)))))

(deftest Web-server-ops
  (testing "Creation properties"
    (let [web (server/create-web-server)]
      (is (instance? Server web))
      (is (number? (server/get-port web)))
      (is (not (nil? (server/get-service web))))
      (is (string? (server/get-status web)))
      (is (= "Not started" (server/get-status web)))
      (is (string? (server/get-URL web)))
      (is (re-matches #"^http://[^:]+:\d+$"
           (server/get-URL web)))
      (is (false? (server/is-running? web)))
      (let [web2 (server/start! web)]
        (is (= web web2))
        (is (true? (server/is-running? web2)))
        (is (re-matches #"^Web Console server running at http://.*$"
                        (server/get-status web2)))
        (is (= web2 (server/stop! web2)))
        (is (= "Not started" (server/get-status web2)))
        (server/start! web2)
        (is (re-matches #"^Web Console server running at http://.*$"
                        (server/get-status web2)))
        (server/stop! web2)))))

