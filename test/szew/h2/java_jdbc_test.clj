; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database java.jdbc administration helpers tests."}
 szew.h2.java-jdbc-test
 (:require
   [clojure.java.jdbc :as jdbc]
   [szew.h2 :refer [spec spec->URL]]
   [szew.h2.java-jdbc
    :refer [set-opts! raze! copy! pump! dump!]]
   [szew.h2.util :refer [de-clob]]
   [clojure.test :refer [deftest testing is]]
   [orchestra.spec.test :as st]
   [szew.h2-test :refer [file-please! thanks-for-the-file!]])
 (:import
   [java.io File]))

(st/instrument)

(deftest Roundtripping-test-with-file-store-databases-java-jdbc

  (let [schema "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"
        record {:a "A" :b "B" :c "C"}
        boop!  (fn [conn]
                 (jdbc/execute! conn [schema])
                 (jdbc/insert! conn :x record)
                 (jdbc/query conn "SELECT * FROM X" {:result-set-fn first}))]
    (try
      (testing "Testing :mem"
        (let [tmp (file-please!) 
              db  (spec (.getName ^File tmp) {:method :mem})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mem-fs"
        (let [tmp (file-please!) 
              db  (spec (.getName ^File tmp) {:method :nio-mem-fs})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mem-lzf"
        (let [tmp (file-please!)
              db  (spec (.getName ^File tmp) {:method :nio-mem-lzf})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mapped"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp) {:method :nio-mapped})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio-mapped with split enabled"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp)
                        {:method :nio-mapped :split? true})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :raf"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp) {:method :raf})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :raf with split enabled"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp)
                        {:method :raf :split? true})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :async"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp) {:method :async})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :async with split enabled"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp)
                        {:method :async :split? true})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp) {:method :nio})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :nio with split"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp)
                        {:method :nio :split? true})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :default"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp) {:method :default})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp)))

      (testing "Testing :default with split"
        (let [tmp (file-please!)
              db  (spec (.getCanonicalPath tmp)
                        {:method :default :split? true})]
          (is (= {:a "A" :b "B" :c "C"} (boop! db)))
          (raze! db)
          (thanks-for-the-file! tmp))))))

(deftest Database-option-setting
  (let [tmp (file-please!)
        db (spec (.getCanonicalPath tmp) {:method :default})]
    (jdbc/with-db-transaction [tx (spec->URL db)]
      (is (nil? (set-opts! tx {})))
      (let [bm (set-opts! tx {"LOCK_MODE" 0 "WRITE_DELAY" 0})
            rm (set-opts! tx {"LOCK_MODE" 3 "WRITE_DELAY" 500})
            xx (set-opts! tx {"BADOPT" 9001})]
        (is (= 2 (count bm)))
        (is (= [0] (get bm "SET LOCK_MODE 0")))
        (is (= [0] (get bm "SET WRITE_DELAY 0")))
        (is (= 2 (count rm)))
        (is (= 1 (count xx)))
        (is (instance? Exception (get xx "SET BADOPT 9001")))))
    (thanks-for-the-file! tmp)))

(deftest Misc-helpers-testing-java-jdbc

  (testing "De-clobbing test."
    (let [db (spec "de-clob-java-jdbc" {:method :mem})]
      (jdbc/with-db-transaction [tx db]
        (let [schema "CREATE TABLE X (A VARCHAR, B CLOB, C CLOB);"
              record {:a "A" :b "B" :c "C"}
              _      (jdbc/execute! tx [schema])
              _      (jdbc/insert! tx :x record)
              read1  (jdbc/query tx ["SELECT * FROM X"] {:result-set-fn first})
              read2  (de-clob read1 [:b]) ; one key
              read3  (de-clob read1)]     ; all keys
          (is (= (:a read1) "A"))
          (is (instance? java.sql.Clob (:b read1)))
          (is (instance? java.sql.Clob (:c read1)))
          (is (= (:a read2) "A"))
          (is (= (:b read2) "B"))
          (is (instance? java.sql.Clob (:c read2)))
          (is (= (:a read3) "A"))
          (is (= (:b read3) "B"))
          (is (= (:c read3) "C"))))
      (raze! db))))

(deftest Pump-n-dump-testing-uncompressed-out-uncompressed-in-java-jdbc

  (let [tmp (file-please!)]
    (try
      (let [dmp    (spec "dmp-java-jdbc" {:method :mem})
            imp    (spec "imp-java-jdbc" {:method :mem})
            schema  "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
        (jdbc/execute! dmp [schema])
        (jdbc/insert! dmp :x {:a "A" :b "B" :c "C"})
        (dump! dmp (.getCanonicalPath tmp) false)
        (pump! imp (.getCanonicalPath tmp) false)
        (is (= (jdbc/query dmp ["SELECT * FROM X"]
                           {:result-set-fn first})
               (jdbc/query imp ["SELECT * FROM X"]
                           {:result-set-fn first})))
        (raze! dmp)
        (is (thrown? Exception
                     (jdbc/query dmp ["SELECT * FROM X"]
                                 {:result-set-fn first}))))
      (finally
        (thanks-for-the-file! tmp)))))

(deftest Pump-n-dump-testing-compressed-out-compressed-in-java-jdbc

  (let [tmp (file-please!)]
    (try
      (let [dmp    (spec "dmp-gz-java-jdbc" {:method :mem})
            imp    (spec "imp-gz-java-jdbc" {:method :mem})
            schema  "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
        (jdbc/execute! dmp [schema])
        (jdbc/insert! dmp :x {:a "A" :b "B" :c "C"})
        (dump! dmp (.getCanonicalPath tmp) true)
        (pump! imp (.getCanonicalPath tmp) true)
        (is (= (jdbc/query dmp ["SELECT * FROM X"]
                           {:result-set-fn first})
               (jdbc/query imp ["SELECT * FROM X"]
                           {:result-set-fn first})))
        (raze! dmp)
        (is (thrown? Exception
                     (jdbc/query dmp ["SELECT * FROM X"]
                                 {:result-set-fn first}))))
      (finally
        (thanks-for-the-file! tmp)))))

(deftest Copy-test-no-pump-n-dump-java-jdbc
  (let [tmp (file-please!)]
    (try
      (let [orig   (spec "orig-copy-java-jdbc" {:method :mem})
            dest   (spec "dest-copy-java-jdbc" {:method :mem})
            schema "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
        (jdbc/execute! orig [schema])
        (jdbc/insert! orig :x {:a "A" :b "B" :c "C"})
        (copy! orig dest (.getCanonicalPath tmp) true)
        (is (= (jdbc/query orig ["SELECT * FROM X"]
                           {:result-set-fn first})
               (jdbc/query dest ["SELECT * FROM X"]
                           {:result-set-fn first})))
        (raze! orig)
        (is (thrown? Exception
                     (jdbc/query orig ["SELECT * FROM X"]
                                 {:result-set-fn first}))))
      (finally
        (thanks-for-the-file! tmp)))))

(deftest Copy-test-no-pump-n-dump-no-file-java-jdbc
  (let [orig   (spec "orig-copy-no-file-java-jdbc" {:method :mem})
        dest   (spec "dest-copy-no-file-java-jdbc" {:method :mem})
        schema "CREATE TABLE X (A VARCHAR, B VARCHAR, C VARCHAR);"]
    (jdbc/execute! orig [schema])
    (jdbc/insert! orig :x {:a "A" :b "B" :c "C"})
    (copy! orig dest true) 
    (is (= (jdbc/query orig ["SELECT * FROM X"]
                       {:result-set-fn first})
           (jdbc/query dest ["SELECT * FROM X"]
                       {:result-set-fn first})))
    (raze! orig)
    (is (thrown? Exception
                 (jdbc/query orig ["SELECT * FROM X"]
                             {:result-set-fn first})))))
