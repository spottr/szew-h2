# szew/h2

Clojure wrapper for H2 database.

[![szew/io](https://clojars.org/szew/h2/latest-version.svg)](https://clojars.org/szew/h2)

[API Codox][latest], [Changelog][changelog]

Breaking changes in 0.3.0

**Table of Contents**

[TOC]

---

## Why

Sometimes you need a disposable relational database connection *now*.
[H2 database][h2] is really handy for in-process, in-memory work on the JVM.

## Usage

Swiftly `spec`ify H2 database connection map. Transform it into URL, Data
Source or Connection Pool in one step.

### Connection, URL, Data Source and Connection Pool

```clojure
user=> (require '[szew.h2 :refer [spec spec->URL spec->DS spec->CP])
nil

user=> (spec "dbname")
{:classname "org.h2.Driver"
 :password ""
 :subname "nio:dbname"
 :subprotocol "h2"
 :user "sa"}

user=> (spec "dbname" {:method :mem})
{:classname "org.h2.Driver"
 :password ""
 :subname "mem:dbname;DB_CLOSE_DELAY=-1"
 :subprotocol "h2"
 :user "sa"}

user=> (spec "dbname" {:method :tcp})
{:classname "org.h2.Driver"
 :password ""
 :subname "tcp://localhost:9092/dbname"
 :subprotocol "h2"
 :user "sa"}

user=> (spec->URL (spec "dbname"))
"jdbc:h2:nio:dbname"

user=> (spec->DS (spec "dbname"))
#object[org.h2.jdbcx.JdbcDataSource 0x740970ab "ds1: url=jdbc:h2: (...)"]

user=> (spec->CP (spec "dbname"))
#object[org.h2.jdbcx.JdbcConnectionPool 0x71cffa53 "org.h2.jdbcx.(...)"]

```

### Connection Parameters

Second argument to `spec`, the `opts` map, is:

```clojure
{:method   :default
 :user     "sa"
 :password ""
 :hostname "localhost"
 :port     9092
 :split?   false
 :part     30
 :flags    {"OPTION" "VALUE"}}
```

The `:method` sets database access mode and can be one of:

```clojure
#{:default
  :nio
  :raw
  :tcp
  :mem
  :nio-mapped
  :nio-mem-fs
  :nio-mem-lzf
  :raf
  :async}
```

The `:default` method is NIO. See `defaults` in `szew.h2` and doc string
of `szew.h2/spec`.

Please keep in mind this library tries to stay as close to most recent release
of the [H2 database][h2] and consult the documentation if you're behind.

### Admin Tasks

You can find some utility wrappers for H2 tasks, like `dump!`, `pump!`,
`raze!`, `shutdown!` and `copy!`, one set per library under `szew.h2.java-jdbc`
and `szew.h2.next-jdbc`.

Remember that including H2 in your application provides you with all the tools
included within that jar. Please consult H2 docs for details.

## Server Modes

H2 comes with several server options included: a PostgreSQL compatibility mode,
a TCP server and a fancy Web Console, which allows to visually inspect
currently running instances. Most methods of `org.h2.tools.Server` class were
wrapped in the `szew.h2.server` namespace.

Server arguments were left "as is", because these are bound to the command
line interface and might vary between H2 versions, so they will depend on your
H2 version.

Reference: [`org.h2.tools.Server` javadoc][h2server].

## Bonus HOWTO: Extending H2 with Clojure

H2 can be extended with Java and we can ride on that with Java interop:

```clojure
(ns my-procedures
  (:gen-class
    :name "my_procedures"
    :main false
    :methods [
     ^{:static true}
     [long2str [java.lang.Long] java.lang.String]
     ^{:static true}
     [xnay [java.sql.Connection java.lang.Long] java.sql.ResultSet]]))


;; This will be a function
(defn -long2str [a-long]
  (str a-long))

;; This stored procedure will just run code present in query q
(defn -xnay [^java.sql.Connection conn ^String q]
  ;;java.sql.ResultSet is expected here
  (.executeQuery (.createStatement conn) q))
```

Then compile the namespace and register in H2 via SQL:

```sql
CREATE ALIAS F FOR "my_procedures.long2str";
CREATE ALIAS P FOR "my_procedures.xnay";
```

If that works -- you can now `call` your code in SQL:

```sql
CALL F(100);
-- => "100"
CALL P('SELECT * FROM SOMETABLE');
-- => results of that select
```

## Migrating from 0.2.x to 0.3.0

Spec storage was removed in version 0.3.0, these are gone:

* `szew.h2/make!`
* `szew.h2/conn!`
* `szew.h2/dispose!`
* `szew.h2/adjust!`
* `szew.h2/connections` storage
* `szew.h2/in-memory!`
* `szew.h2/local!`

Support for `next.jdbc` was introduced, so convenience functions to export,
import, clear and copy databases got extracted into `szew.h2.java-jdbc`
and `szew.h2.next-jdbc`. These are:

* `set-opts!`
* `bulk-insert-mode!`
* `retrieval-mode!`
* `dump!`
* `pump!`
* `raze!`
* `shutdown!`
* `copy!`

The misc helpers `szew.h2/de-clob` and `szew.h2/columnlist?` were moved into
`szew.h2.util` namespace.

The `:part` parameter was changed from 2GiB to 1GiB. Also it means something
special for nioMemLZF: percentage of uncompressed data blocks.

## License

Copyright © 2012--2023 Sławek Gwizdowski

MIT License, text can be found in the LICENSE file.

[latest]: https://spottr.bitbucket.io/szew-h2/latest/
[changelog]: CHANGELOG.md
[h2]: https://h2database.com/
[h2docs]: https://h2database.com/html/quickstart.html
[h2server]: http://h2database.com/javadoc/org/h2/tools/Server.html
