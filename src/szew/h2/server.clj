; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc
"H2 database server mode and web console functions.

See: http://h2database.com/javadoc/org/h2/tools/Server.html"}
 szew.h2.server
 (:require
   [clojure.spec.alpha :as s])
 (:import [java.sql Connection]
          [org.h2.tools Server]))

;; H2 Servers wrapper

;; See: http://h2database.com/javadoc/org/h2/tools/Server.html

(defn create-pg-server
  "Creates PG server, does not start it, just returns primed new instance.

  Accepts command-line-like argument list as vector, see H2 doc for details:

  http://h2database.com/javadoc/org/h2/tools/Server.html

  Some options that should be available:

  [-pgPort] to set port for the server
  [-pgAllowOthers] to allow connections from outside localhost
  [-pgDaemon] to use a Daemon thread
  [-trace] for verbose STDOUT event printing
  [-ifExists] only existing databases will be used
  [-ifNotExists] will create a new database on access
  [-baseDir] the base directory for databases
  [-key [from to]] to map database name to another.
  "
  ([args]
   (org.h2.tools.Server/createPgServer (into-array String (map str args))))
  ([]
   (create-pg-server [])))

(s/fdef create-pg-server
  :args (s/alt :1-arg  (s/cat :args (s/coll-of string? :into []))
               :0-args (s/cat))
  :ret  (partial instance? Server))

(defn create-tcp-server
  "Creates TCP server, does not start it, just returns primed new instance.

  Accepts command-line-like argument list as vector, see H2 doc for details:

  http://h2database.com/javadoc/org/h2/tools/Server.html

  Some options that should be available:

  [-tcpPort] to set port for the server
  [-tcpSSL] to enable SSL connections
  [-tcpPassword [pwd]] password for the DB Console administrator access
  [-tcpAllowOthers] to allow connections from outside localhost
  [-tcpDaemon] to use a Daemon thread
  [-trace] for verbose STDOUT event printing
  [-ifExists] only existing databases will be used
  [-ifNotExists] will create a new database on access
  [-baseDir] the base directory for databases
  [-key [from to]] to map database name to another.
  "
  ([args]
   (org.h2.tools.Server/createTcpServer (into-array String (map str args))))
  ([]
   (create-tcp-server [])))

(s/fdef create-tcp-server
  :args (s/alt :1-arg  (s/cat :args (s/coll-of string? :into []))
               :0-args (s/cat))
  :ret  (partial instance? Server))

(defn create-web-server
  "Creates Web server, does not start it, just returns primed new instance.

  Accepts command-line-like argument list as vector, see H2 doc for details:

  http://h2database.com/javadoc/org/h2/tools/Server.html

  Some options that should be available:

  [-webPort] to set port for the server
  [-webSSL] to enable SSL connections
  [-webAdminPassword [pwd]] password for the DB Console administrator access
  [-webAllowOthers] to allow connections from outside localhost
  [-webDaemon] to use a Daemon thread
  [-browser] try to start a browser connecting to DB Console
  [-trace] for verbose STDOUT event printing
  [-ifExists] only existing databases will be used
  [-ifNotExists] will create a new database on access
  [-baseDir] the base directory for databases
  [-key [from to]] to map database name to another.
  "
  ([args]
   (org.h2.tools.Server/createWebServer (into-array String (map str args))))
  ([]
   (create-web-server [])))

(s/fdef create-web-server
  :args (s/alt :1-arg  (s/cat :args (s/coll-of string? :into []))
               :0-args (s/cat))
  :ret  (partial instance? Server))

(defn get-port
  "Returns port the server is running on.
  "
  [^Server server]
  (.getPort server))

(s/fdef get-port
  :args (s/cat :server (partial instance? Server))
  :ret  number?)

(defn get-service
  "Returns service attached to the server.
  "
  [^Server server]
  (.getService server))

(s/fdef get-service
  :args (s/cat :server (partial instance? Server)))

(defn get-status
  "Returns status of the server.
  "
  [^Server server]
  (.getStatus server))

(s/fdef get-status
  :args (s/cat :server (partial instance? Server))
  :ret  string?)

(defn get-URL
  "Returns URL of the server.
  "
  [^Server server]
  (.getURL server))

(s/fdef get-URL
  :args (s/cat :server (partial instance? Server))
  :ret  string?)

(defn is-running?
  "Returns true if server is up, false otherwise.
  "
  ([^Server server ^Boolean trace-error]
   (.isRunning server trace-error))
  ([^Server server]
   (.isRunning server false)))

(s/fdef is-running?
  :args
  (s/alt :2-args (s/cat :server (partial instance? Server) :flag boolean?)
         :1-arg  (s/cat :server (partial instance? Server)))
  :ret boolean?)

(defn start!
  "Starts the server and returns it.
  "
  [^Server server]
  (io! "Side effects here!"
       (.start server)))

(s/fdef start!
  :args (s/cat :server (partial instance? Server))
  :ret  (partial instance? Server))

(defn stop!
  "Stops the server and returns it.
  "
  [^Server server]
  (io! "Side effects here!"
       (.stop server)
       server))

(s/fdef stop!
  :args (s/cat :server (partial instance? Server))
  :ret  (partial instance? Server))

;; Debugging helpers: Server started based on connection

(defn start-web-server
  "Creates a Web server based on provided connection, with current transaction
  preserved. Opens web browser DB Console with appropriate session.

  Purpose: manually inspect database state during debugging.

  == !WARNING! ==
  Blocks until user disconnects explicitly from the Web Console!

  Closing the web browser is not it - MUST click 'Disconnect' within the UI!
  == !WARNING! ==
  "
  ([^Connection connection ^Boolean ignore-properties]
   (io! "Impurity! And blocking!"
        (org.h2.tools.Server/startWebServer connection ignore-properties)))
  ([^Connection connection]
   (start-web-server connection false)))
