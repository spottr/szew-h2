; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database java.jdbc administration helpers."}
 szew.h2.java-jdbc
 (:require
   [clojure.java.jdbc :as jdbc])
 (:import
   [java.io File]))

;; ## Insert and read modes.


(defn set-opts!
  "Set H2 options from a map. One by one, return results.
  "
  [conn opts-map]
  (when-not (empty? opts-map)
    (let [fmt  #(str "SET " (first %1) " " (second %1))
          exe #(try (jdbc/execute! conn %1)
                    (catch Exception ex ex))]
      (->> (mapv fmt opts-map)
           (mapv vector)
           (mapv (juxt first exe))
           (into {})))))

(defn bulk-insert-mode!
  "Set H2 into bulk insert mode recommended at one time in docs.

  See also: LOCK_MODE, WRITE_DELAY, MAX_OPERATION_MEMORY & CACHE_SIZE.
  Use set-opts accordingly."
  [conn]
  (set-opts! conn {"LOCK_MODE" 0 "WRITE_DELAY" 0}))

(defn retrieval-mode!
  "Set H2 into settings to defaults.

  See also: LOCK_MODE, WRITE_DELAY, MAX_OPERATION_MEMORY & CACHE_SIZE.
  Use set-opts accordingly."
  [conn]
  (set-opts! conn {"LOCK_MODE" 3 "WRITE_DELAY" 500}))

;; ## Maintenance tools

(defn dump!
  "Exports database into a SQL file (server side).
  "
  ([conn dump-file compression?]
   (io! "IO happens."
        (if compression?
          (jdbc/query conn
                      ["SCRIPT TO ? COMPRESSION GZIP;" dump-file]
                      {:result-set-fn vec})
          (jdbc/query conn
                      ["SCRIPT TO ?;" dump-file]
                      {:result-set-fn vec})))))

(defn pump!
  "Imports database from SQL file (server side).
  "
  ([conn dump-file compression?]
   (io! "IO happens."
        (if compression?
          (jdbc/execute! conn ["RUNSCRIPT FROM ? COMPRESSION GZIP;" dump-file])
          (jdbc/execute! conn ["RUNSCRIPT FROM ?;" dump-file])))))

(defn raze!
  "Drops all objects and, if no connections are open, deletes database files.

  Usually in embedded mode executing this removed the database files.
  "
  ([conn]
   (io! "IO happens."
        (jdbc/execute! conn ["DROP ALL OBJECTS DELETE FILES;"]))))

(defn shutdown!
  "Executes SHUTDOWN in connected database, requires admin rights.

  The option argument may be: :immediately, :compact or :defrag. By default
  normal shutdown (no switch) is executed.
  "
  ([conn option]
   (io! "IO happens."
        (case option
          :immediately
          (jdbc/execute! conn ["SHUTDOWN IMMEDIATELY;"])
          :compact
          (jdbc/execute! conn ["SHUTDOWN COMPACT;"])
          :defrag
          (jdbc/execute! conn ["SHUTDOWN DEFRAG;"])
          (jdbc/execute! conn ["SHUTDOWN;"]))))
  ([conn]
   (shutdown! conn :just-go)))

(defn copy!
  "Will dump! source, raze! target, pump! target.

  If dump-file not given uses temporary file and deletes it afterwards.
  "
  ([source target dump-file compression?]
   (io! "IO on top of IO."
        (dump! source dump-file compression?)
        (raze! target)
        (pump! target dump-file compression?)))
  ([source target compression?]
   (io! "IO on top of IO."
        (let [storage   (File/createTempFile "szew-copy-" ".bin")
              dump-file (.getCanonicalPath ^File storage)]
          (try
            (dump! source dump-file compression?)
            (raze! target)
            (pump! target dump-file compression?)
            (finally (.delete ^File storage)))))))
