; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database connection specification."}
  szew.h2.spec
  (:require
    [clojure.spec.alpha :as s]))

;; ## Default settings used by spec

(s/def ::method #{:default :nio :raw :tcp :mem :nio-mapped :nio-mem-fs
                  :nio-mem-lzf :raf :async})

(s/def ::user string?)

(s/def ::password string?)

(s/def ::hostname string?)

(s/def ::port (s/and int? pos?))

(s/def ::split? boolean?)

(s/def ::part (s/and int? pos?))

(s/def ::flags (s/map-of string? string?))

(s/def ::opts
  (s/nilable
    (s/keys :opt-un [::method ::user ::password ::hostname ::port ::split?
                     ::part ::flags])))

(s/def ::classname (partial = "org.h2.Driver"))

(s/def ::subprotocol (partial = "h2"))

(s/def ::subname string?)

(s/def ::spec
  (s/keys :req-un [::classname ::subprotocol ::subname ::user ::password]))
