; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database client utility functions."}
 szew.h2.util
 (:require
   [clojure.java.io :as clj.io]
   [clojure.spec.alpha :as s])
 (:import [java.sql Clob Connection]))

;; Misc helpers.

(defn de-clob
  "CLOB to String conversion in a-map returned from the database.

  Warning: database connection must be open -- run in transaction.
  "
  ([a-map touch-keys]
   (->> (select-keys a-map touch-keys)
        (filter (comp (partial instance? Clob) val))
        (map (fn [[key ^Clob clob]]
               (with-open [r (clj.io/reader (.getCharacterStream clob))]
                 [key (slurp r)])))
        (into a-map)))
  ([a-map]
   (de-clob a-map (keys a-map))))

(s/fdef de-clob
  :args
  (s/alt :2-args (s/cat :a-map map? :touch-keys (s/or :vec vector? :seq seq?))
         :1-arg  (s/cat :a-map map?))
  :ret map?)

;; ## For interop in stored procedures etc.

(defn columnlist?
  "Are we being asked for columnlist spec?

  This will return true if inside a function being used as stored procedure
  by H2 engine. It means that we are requested to return java.sql.ResultSet,
  like org.h2.tools.SimpleResultSet (for queries) or Java type mapped by H2
  for functions.
  "
  [^Connection conn]
  (let [url (.getURL (.getMetaData conn))]
    (= "jdbc:columnlist:connection" url)))
