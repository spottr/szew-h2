; Copyright (c) Sławek Gwizdowski
;
; Permission is hereby granted, free of charge, to any person obtaining
; a copy of this software and associated documentation files (the "Software"),
; to deal in the Software without restriction, including without limitation
; the rights to use, copy, modify, merge, publish, distribute, sublicense,
; and/or sell copies of the Software, and to permit persons to whom the
; Software is furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included
; in all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
; OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
; THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
; IN THE SOFTWARE.
;
(ns ^{:author "Sławek Gwizdowski"
      :doc "H2 database connection setup helper functions."}
  szew.h2
  (:require
    [szew.h2.spec :as sp]
    [clojure.string :as string]
    [clojure.spec.alpha :as s])
  (:import
    [org.h2.jdbcx JdbcDataSource JdbcConnectionPool]))

(def
  ^{:doc "Default opts for MVStore."}
  defaults
  {:method   :default
   :user     "sa"
   :password ""
   :hostname "localhost"
   :port     9092
   :split?   false
   :part     30 ;; 1GiB, 2**30 bytes
   :flags    {}})

(defn spec
  "Create H2 connection spec for given path String (only required argument).

  Store depends on method parameter of opts:

  * :raw sets :subname to given path verbose (best of luck!)
  * :tcp uses server, port and path for :subname
  * :mem sets database to in-memory storage, JVM runtime persistent
  * :nio-mem-fs is the new in-memory storage, JVM runtime persistent
  * :nio-mem-lzf for new in-memory compressing storage, JVM runtime persistent
  * :raf sets database to embedded, RandomAccessFile local storage
  * :async uses AsyncChannel instead of RandomAccessFile, may be faster
  * :nio uses Java nio instead of RandomAccessFile, may be faster
  * :nio-mapped uses nio over mem-mapped file, 2GB max size (split implied)
  * :default or anything else means local embedded (nio) file storage.

  Other opts are :user, :password, :server & :port. Self explanatory.

  For split filesystem use :split? flag and (2 ** :part) bytes for size. This
  mode can be stacked with some filesystem modes.

  For :nio-mem-lzf the :part argument is used as percentage of blocks that should
  be kept uncompressed, so ideally it's between 1 and 99.

  :flags should be a map of {String String}, like {COMPRESS TRUE}, it will
  overwrite *all* flags set by default so YMMV.

  In-memory stores are opened with DB_CLOSE_DELAY=-1 by default, which means
  they will be available until JVM shuts down. You can close them with SHUTDOWN
  command.
  "
  ([path opts]
   {:pre  [(string? path)
           (or (s/valid? ::sp/opts opts) (s/explain ::sp/opts opts))]
    :post [(or (s/valid? ::sp/spec %) (s/explain ::sp/spec %))]}
   (let [merged   (merge defaults opts)
         {:keys [method user password hostname port flags split? part]} merged
         tail    (if (seq flags)
                   (->> flags
                        (map (partial apply (partial format "%s=%s")))
                        (string/join ";")
                        (str ";"))
                   "")]
     {:classname   "org.h2.Driver"
      :subprotocol "h2"
      :subname     (case method
                     ; ganbare!
                     :raw
                     path
                     ; remote H2 instance
                     :tcp
                     (format "tcp://%s:%d/%s%s" hostname port path tail)
                     ; in-memory alive until JVM exits
                     :mem
                     (format "mem:%s;DB_CLOSE_DELAY=-1%s" path tail)
                     ; nio in-memory alive until JVM exits
                     :nio-mem-fs
                     (format "nioMemFS:%s;DB_CLOSE_DELAY=-1%s" path tail)
                     ; compressing nio in-memory alive until JVM exits
                     :nio-mem-lzf
                     (format "nioMemLZF:%d:%s;DB_CLOSE_DELAY=-1%s"
                             part path tail)
                     ; nio with memory mapped file
                     :nio-mapped
                     (format "split:%d:nioMapped:%s%s" part path tail)
                     ; RandomAccessFile based database (non-NIO)
                     :raf
                     (if split?
                       (format "split:%d:%s%s" part path tail)
                       (format "%s%s" path tail))
                     ; AsyncChannel instead of RandomAccessFile
                     :async
                     (if split?
                       (format "split:%d:async:%s%s" part path tail)
                       (format "async:%s%s" path tail))
                     ; NIO explicitly
                     :nio
                     (if split?
                       (format "split:%d:nio:%s%s" part path tail)
                       (format "nio:%s%s" path tail)) 
                     ; :default, standard in-file persistence using NIO
                     (if split?
                       (format "split:%d:nio:%s%s" part path tail)
                       (format "nio:%s%s" path tail)))
      :user        user
      :password    password}))
  ([path]
   {:pre  [(string? path)]
    :post [(or (s/valid? ::sp/spec %) (s/explain ::sp/spec %))]}
   (spec path nil)))

(s/fdef spec
  :args (s/alt
          :1-arg  (s/cat :path string?)
          :2-args (s/cat :path string? :spec ::sp/opts))
  :ret  ::sp/spec)

;; spec transformations!

(defn spec->URL
  "Eats spec, returns URL.
 
  Requires :subname.
  "
  [a-spec]
  {:pre [(or (s/valid? ::sp/spec a-spec) (s/explain ::sp/spec a-spec))]}
  (str "jdbc:h2:" (:subname a-spec)) )

(defn spec->DS
  "Eats spec, returns org.h2.jdbcx.JdbcDataSource.

  Requires :subname, :user and :password.
  "
  [a-spec]
  {:pre [(or (s/valid? ::sp/spec a-spec) (s/explain ::sp/spec a-spec))]}
  (doto (JdbcDataSource.)
    (.setURL ^String (spec->URL a-spec))
    (.setUser ^String (:user a-spec))
    (.setPassword ^String (:password a-spec))))

(defn spec->CP
  "Eats spec, returns org.h2.jdbcx.JdbcConnectionPool.

  Requires :subname, :user and :password.
  "
  [a-spec]
  {:pre [(or (s/valid? ::sp/spec a-spec) (s/explain ::sp/spec a-spec))]}
  (JdbcConnectionPool/create ^JdbcDataSource (spec->DS a-spec)))
