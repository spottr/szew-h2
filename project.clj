(defproject szew/h2 "0.4.1-SNAPSHOT"

  :description "Clojure wrapper for H2 database."
  :url "https://bitbucket.org/spottr/szew-h2"

  :license {:name "MIT Public License"
            :distribution :repo
            :comments "LICENSE file in project root directory."}

  :dependencies []

  :profiles {:dev      {:dependencies [[criterium "0.4.6"]
                                       [orchestra "2021.01.01-1"]]
                        :eftest {:thread-count 2}
                        :plugins [[lein-codox "0.10.8"]]
                        :source-paths ["dev"]}
             :provided {:dependencies [[org.clojure/clojure "1.12.0"]
                                       [com.h2database/h2 "2.3.232"]
                                       [org.clojure/java.jdbc "0.7.12"]
                                       [com.github.seancorfield/next.jdbc "1.3.981"]]}
             :1.12     {:dependencies [[org.clojure/clojure "1.12.0-beta1"]]}
             :uberjar  {:aot :all}}

  :global-vars {#_#_*warn-on-reflection* true
                #_#_*assert* true}

  :aot [szew.h2
        szew.h2.util
        szew.h2.server]

  :codox {;:metadata {:doc/format :markdown}
          :project {:name "szew/h2"}
          :namespaces [#"^(szew\.h2|szew\.h2\.(?:java|next|serv|util).*)$"]
          :source-uri ~(str "https://bitbucket.org/spottr/szew-h2/src/"
                            "ddb38c1f35e796f4fedf459906601cb837c9f719" ;; 0.4.0
                            "/{filepath}#{basename}-{line}")}
  )

